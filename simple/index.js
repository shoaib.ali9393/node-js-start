 const express = require('express');
 const ProductController = require('./controllers/ProductController');
 const app = express();


 
 app.get('/' , (req , resp )=>{
          resp.send(`<h2>welcome to home page</h2>`)
 });

 app.get('/getProduct' , (req , resp )=>{
    ProductController.fetchData(req.query).then((data)=>{
          resp.send(data);
     });
});

app.get('/insertProduct' , (req , resp )=>{
    ProductController.insertData(req.query).then((data)=>{
         resp.send(data);
    });
});

app.get('/updateProduct' , (req , resp )=>{
    ProductController.updateData(req.query).then((dataa)=>{
               resp.send(dataa);
        });
});

app.get('/deleteProduct' , (req , resp )=>{
    ProductController.deleteData(req.query).then((dataa)=>{
           resp.send(dataa);
    });
});


// const express = require('express');
// const path = require('path');
// require('./middlewares/one');


// const route = express.Router();
// const app = express();
// route.use(ageCheck);
// // app.use(ageCheck)
// app.set('view engine', 'ejs');
// const viewsPath = path.join(__dirname, 'views');
// // app.use(express.static(viewsPath));



// app.get('', (req, resp) => {
//     // resp.sendFile(viewsPath+"/home.html");
//     resp.sendFile(`${viewsPath}/home.html`);
// });

// route.get('/profile', (req, resp) => {
//     const data = {
//         name: "shoaib",
//         phone: '03047499393',
//         age: 28,
//         skills: ['laravel', 'js', 'node'],
//     }
//     resp.render('profile', { data });
// });

// app.use('/', route);
app.listen(5000);