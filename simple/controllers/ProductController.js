const dbConnect  = require('../config/mongo');

async function fetchData(){
    let result = await (dbConnect.dbConnect());
    let data = await result.find().toArray();
    return data;
 }

 async function insertData(req){
    let result = await (dbConnect.dbConnect());
    let data = await result.insertOne(req);
    return data;
 }

 async function updateData(req){
    let result = await (dbConnect.dbConnect());
    let data = await result.updateOne({name: req.name}, {$set:{name:req.upname , age : req.age}});
    return data;
 }

 async function deleteData(req){
    let result = await (dbConnect.dbConnect());
    let data = await result.deleteOne({name: req.name});
    return data;
 }

 module.exports = {
    fetchData,
    insertData,
    updateData,
    deleteData,

 }