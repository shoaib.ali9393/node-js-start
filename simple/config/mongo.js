const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const DBName = 'nodejs';
const collectionName = 'product';

async function dbConnect() {
    let connectionDb = await client.connect();
    let db = await connectionDb.db(DBName);
    return db.collection(collectionName);
}

module.exports = {
    dbConnect,
};