const express = require('express');
const app = express();
app.use(express.json());
const database = require('../controllers/ProductController');

app.get('/api/index' , async (req,resp) => {
        const data =  await database.fetchData();
        console.log(data);
        resp.send(data);
});


app.post('/api/save' , async (req,resp) => {
    const result =  await database.insertData(req.body);
    resp.send(result);
});

app.get('*', (req , resp)=>{
        resp.send("No api Found");
})

app.listen(5000);