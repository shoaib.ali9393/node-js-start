const express = require('express');
const app = express();
app.use(express.json());
const action = require('./Controller/actions');
const multer = require('multer');
const path = require('path');


const uploadImage = multer({
     storage : multer.diskStorage({
       destination : function(req,file,callBack){
              callBack(null , 'uploads/')
       },
       filename  : function(req,file,callBack){
        callBack(null, file.originalname+Date.now()+path.extname(file.originalname))
       }
     })
}).single('fileupload');


app.get('/', async (req, resp) => {
    var result = await action.save();
    resp.send(result);
});
app.get('/update', async (req, resp) => {
    var result = await action.update();
    resp.send(result);
});

app.post('/api/save', async (req, resp) => {
    var result = await  action.apiSave(req.body);
    resp.send(result);

});

app.get('/api/search/', async (req, resp) => {
    var result = await  action.apiSearch(req.query);
    resp.send(result);

});
uploadImage

app.post('/api/upload/',(req, resp) => {
    resp.send(req.body);

});

app.listen(5000);