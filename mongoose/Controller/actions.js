const express = require('express');
const app = express();
app.use(express.json());
const modalOBJ = require('../migration/products');


const save = async () => {
    const result = await new modalOBJ({ first_name: "mongoose1", last_name: "check" });
    const data = await result.save();
    return data;
}
const update = async () => {
    const result = await modalOBJ.updateMany({ first_name: "mongoose1" }, { $set: { first_name: "mongoose3", last_name: "check3" } });
    const data = result;
    return data;
}

async function apiSave(req) {
    const result = await new modalOBJ({ first_name: req.name, last_name: req.name });
    const data = await result.save();
    return result;
};

async function apiSearch(req) {
    const result = await modalOBJ.find(
        {
            "$or": [
                {"first_name":{$regex:req.first_name}},
                {"last_name":{$regex:req.first_name}},
            ],
            "$or": [
                {"last_name":{$regex:req.last_name}},
            ]
        },
        
    )
    return result;
}


module.exports = {
    save,
    update,
    apiSave,
    apiSearch,
}