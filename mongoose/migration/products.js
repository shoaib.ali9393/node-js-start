const mongoose = require('mongoose');
require('../config/mongoose');

const tableName = "products";

const productScheme = new mongoose.Schema({
    first_name: String,
    last_name: String
})
const modal = mongoose.model(tableName, productScheme);

module.exports = modal;
